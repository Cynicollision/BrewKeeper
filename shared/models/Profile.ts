export interface Profile {
    id: string;
    externalID: string;
    name: string;
}